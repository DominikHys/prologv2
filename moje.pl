% Dominik Hys 334554
:- use_module(library(lists)).

nieusuwalna_rekursja_w_tranzycji(prod(N, Produkcja)) :- member([nt(N)], Produkcja).

% zamienia rekursje na nowy nieterminal (nieterminal, produkcja, acc - zaczyna od N, acc - nie zaczyna od N)
usun_rekursje_z_produkcji(_,[],[],[]).
usun_rekursje_z_produkcji(N, [[nt(N)|ResztaTermu]|ResztaProdukcji], [NowyTerm|PierwszeN], NiepierwszeN) :-
	atom_concat(N,'1',N1),
	append(ResztaTermu,[nt(N1)],NowyTerm),
	usun_rekursje_z_produkcji(N,ResztaProdukcji,PierwszeN,NiepierwszeN).
usun_rekursje_z_produkcji(N,[[]|ResztaProdukcji],[N1|PierwszeN],NiepierwszeN) :-
	atom_concat(N,'1',N1),
	usun_rekursje_z_produkcji(N,ResztaProdukcji,PierwszeN,NiepierwszeN).
usun_rekursje_z_produkcji(N,[[GlowaTermu|ResztaTermu]|ResztaProdukcji],PierwszeN,[NowyTerm|NiepierwszeN]) :-
	GlowaTermu \= nt(N),
	atom_concat(N,'1',N1),
	append([GlowaTermu|ResztaTermu],[nt(N1)],NowyTerm),
	usun_rekursje_z_produkcji(N,ResztaProdukcji,PierwszeN,NiepierwszeN).

nie_potrzeba_usuwac_rekursji_z_produkcji(N, Produkcja) :-
	usun_rekursje_z_produkcji(N,Produkcja,PierwszeN,_),
	PierwszeN == [].

dodaj_epsilon(PierwszeN, [[]|PierwszeN]).

usun_rekursje_z_tranzycji(prod(N,Produkcja), prod(N,Produkcja)) :-
	nie_potrzeba_usuwac_rekursji_z_produkcji(N,Produkcja).
usun_rekursje_z_tranzycji(prod(N,Produkcja), [prod(N,NiepierwszeN),prod(N1,PierwszeNEps)]) :-
	usun_rekursje_z_produkcji(N,Produkcja, PierwszeN, NiepierwszeN),
	atom_concat(N,'1',N1),
	PierwszeN \= [],
	NiepierwszeN \= [],
	dodaj_epsilon(PierwszeN,PierwszeNEps).

usun_rekursje_z_gramatyki([],[]).
usun_rekursje_z_gramatyki([Tranzycja|ResztaTranzycji], [TranzycjaWyjsciowa|ResztaWyjscia]) :-
	\+ nieusuwalna_rekursja_w_tranzycji(Tranzycja),
	usun_rekursje_z_tranzycji(Tranzycja, TranzycjaWyjsciowa),
	usun_rekursje_z_gramatyki(ResztaTranzycji, ResztaWyjscia).

% dziala
remDirectLeftRec([], []).
remDirectLeftRec(GramatykaWejsciowa, GramatykaWyjsciowa) :-
	usun_rekursje_z_gramatyki(GramatykaWejsciowa, GramatykaWyjsciowa).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nieterminale_bezp_na_epsilon([],[]).
nieterminale_bezp_na_epsilon([prod(N, Tranzycja)|ResztaTranzycji], [N|NaEpsilon]) :-
	member([],Tranzycja),
	nieterminale_bezp_na_epsilon(ResztaTranzycji,NaEpsilon).
nieterminale_bezp_na_epsilon([prod(_, Tranzycja)|ResztaTranzycji], NaEpsilon) :-
	\+ member([],Tranzycja),
	nieterminale_bezp_na_epsilon(ResztaTranzycji,NaEpsilon).

produkcje_dla_nieterminali([],_,[]).
produkcje_dla_nieterminali([nt(N)|ResztaNieterminali],Gramatyka,[Produkcja|ResztaWyniku]) :-
	produkcja_dla_nieterminala(N,Gramatyka,Produkcja),
	produkcje_dla_nieterminali(ResztaNieterminali,Gramatyka,ResztaWyniku).

% sprawdza czy produkcja danego nieterminala przechodzi na epsilon 
przechodziNaEpsilon(Gramatyka, prod(N,Produkcja), BezposrednioNaEpsilon) :-
		member(N,BezposrednioNaEpsilon); % przechodzi bezposrednio na epsilon
		 % albo 
		member(N1,Produkcja), % istnieje taka podukcja, ze na nią przechodzi
		produkcje_dla_nieterminali(N1,Gramatyka,ProdukcjaN1), %i ona ma produkcje
		wszystkiePrzechodzaNaEpsilon(Gramatyka,ProdukcjaN1). % ktora przechodzi na epsilon

% sprawdza czy wszystkie produkcje danych nieterminali przechodza na epsilon 
wszystkiePrzechodzaNaEpsilon(_,[]).
wszystkiePrzechodzaNaEpsilon(Gramatyka,[prod(N,Produkcja)|ResztaProdukcji]) :-
		przechodziNaEpsilon(Gramatyka,prod(N,Produkcja)),
		wszystkiePrzechodzaNaEpsilon(Gramatyka,ResztaProdukcji).

% wypisuje liste wszystkich nieterminali, ktore przechodza na epsilon posrednio albo bezposrednio (gramatyka, lista produkcji, lista wyjsciowa) 
przechodzaNaEpsilon(_,[],[], _).
przechodzaNaEpsilon(Gramatyka, [prod(N,Produkcja)|ResztaProdukcji], [N|ResztaWyniku], BezposrednioNaEpsilon) :- 
	przechodziNaEpsilon(Gramatyka,prod(N,Produkcja),BezposrednioNaEpsilon),
	przechodzaNaEpsilon(Gramatyka, ResztaProdukcji, ResztaWyniku, BezposrednioNaEpsilon).
przechodzaNaEpsilon(Gramatyka, [prod(N,Produkcja)|ResztaProdukcji], ResztaWyniku, BezposrednioNaEpsilon) :- 
	\+ przechodziNaEpsilon(Gramatyka, prod(N,Produkcja),BezposrednioNaEpsilon),
	przechodzaNaEpsilon(Gramatyka, ResztaProdukcji, ResztaWyniku, BezposrednioNaEpsilon).

members([],_).
members([Pierwszy|Reszta],Zbior) :-
	member(Pierwszy,Zbior),
	members(Reszta,Zbior).

wyjmij_dane([],[]).
wyjmij_dane([nt(A)|Reszta],[A|ResztaWyniku]) :-
	wyjmij_dane(Reszta,ResztaWyniku).

nieterminale_na_epsilon(Gramatyka,NaEpsilon) :-
    przechodza_na_epsilon(Gramatyka,[],WynikNT), 
    wyjmij_dane(WynikNT,NaEpsilon), !.

przechodza_na_epsilon(Gramatyka,Wynik,NaEpsilon) :- 
    przechodzi_na_epsilon(Gramatyka,Wynik,WynikObrot),
    (Wynik == WynikObrot -> 
    	NaEpsilon = WynikObrot; 
    	przechodza_na_epsilon(Gramatyka,WynikObrot,NaEpsilon)).

przechodzi_na_epsilon([],Term,Term).
przechodzi_na_epsilon([prod(N,Produkcja)|ResztaTranzycji], Wynik,WynikObrot) :- 
    (\+ produkcja_na_epsilon(Produkcja,Wynik); 
    member(nt(N),Wynik)),
    przechodzi_na_epsilon(ResztaTranzycji,Wynik,WynikObrot).
przechodzi_na_epsilon([prod(N,Produkcja)|ResztaTranzycji],Wynik,[nt(N)|WynikObrot]) :-
    \+ member(nt(N),Wynik),
    produkcja_na_epsilon(Produkcja,Wynik),
    przechodzi_na_epsilon(ResztaTranzycji,Wynik,WynikObrot). 

produkcja_na_epsilon([Term|Produkcja],Wynik) :-
    przechodzi_na_epsilon_bezposrednio([Term|Produkcja]);
    tylko_nieterminale(Term),
    members(Term,Wynik);
    produkcja_na_epsilon(Produkcja,Wynik).

tylko_nieterminale([]).
tylko_nieterminale([nt(_)|Reszta]) :-
    tylko_nieterminale(Reszta).

przechodzi_na_epsilon_bezposrednio(Produkcja) :-
    member([], Produkcja).

term_tylko_nieterminale([]).
term_tylko_nieterminale([nt(_)|ResztaTermu]) :-
	term_tylko_nieterminale(ResztaTermu).

produkcja_usun_terminale([],[]).
produkcja_usun_terminale([Term|ResztaProdukcji],[Term|ProdukcjaWynik]) :-
	term_tylko_nieterminale(Term),
	produkcja_usun_terminale(ResztaProdukcji,ProdukcjaWynik).
produkcja_usun_terminale([Term|ResztaProdukcji],ProdukcjaWynik) :-
	\+ term_tylko_nieterminale(Term),
	produkcja_usun_terminale(ResztaProdukcji, ProdukcjaWynik).

gramatyka_usun_terminale([],[]).
gramatyka_usun_terminale([prod(N,Produkcja)|ResztaTranzycji], [prod(N,ProdukcjaWynik)|ResztaWyniku]) :-
	produkcja_usun_terminale(Produkcja, ProdukcjaWynik),
	ProdukcjaWynik \= [],
	gramatyka_usun_terminale(ResztaTranzycji,ResztaWyniku).
gramatyka_usun_terminale([prod(_,Produkcja)|ResztaTranzycji], ResztaWyniku) :-
	produkcja_usun_terminale(Produkcja, ProdukcjaWynik),
	ProdukcjaWynik == [],
	gramatyka_usun_terminale(ResztaTranzycji,ResztaWyniku).

% dajemy tylko listę nieterminali wejściowych i listę nieterminali przechodzących na epsilon
wszystkie_na_epsilon([],_).
wszystkie_na_epsilon([nt(N)|ResztaNieterminali], NaEpsilon) :-
	member(N,NaEpsilon),
	wszystkie_na_epsilon(ResztaNieterminali,NaEpsilon).

% tylko jesli wszystkie nieterminale przechodza na epsilon
rozdziel_przechodzace_na_epsilon([],[]).
rozdziel_przechodzace_na_epsilon([N|ResztaNieterminali], [[N]|ResztaWyniku]) :-
	rozdziel_przechodzace_na_epsilon(ResztaNieterminali,ResztaWyniku).

% podac liste nieterminali i liste nieterminali przechodzacych na epsilon oraz akumulator
ile_nie_na_epsilon([],_,[]).
ile_nie_na_epsilon([nt(N)|ResztaNieterminali], NaEpsilon, [nt(N)|ResztaWyniku]) :-
	\+ member(N,NaEpsilon),
	ile_nie_na_epsilon(ResztaNieterminali,NaEpsilon,ResztaWyniku).
ile_nie_na_epsilon([nt(N)|ResztaNieterminali], NaEpsilon, ResztaWyniku) :-
	member(N,NaEpsilon),
	ile_nie_na_epsilon(ResztaNieterminali,NaEpsilon,ResztaWyniku).

% juz na usunietych terminalach
przerob_produkcje([], _, []).
przerob_produkcje([Term|ResztaProdukcji], NaEpsilon, Wynik) :-
	wszystkie_na_epsilon(Term, NaEpsilon),
	rozdziel_przechodzace_na_epsilon(Term,TermNowy),
	append(TermNowy,ResztaWyniku, Wynik),
	przerob_produkcje(ResztaProdukcji,NaEpsilon,ResztaWyniku).
przerob_produkcje([Term|ResztaProdukcji], NaEpsilon, [NieNaEpsilon|ResztaWyniku]) :-
	\+ wszystkie_na_epsilon(Term, NaEpsilon),
	ile_nie_na_epsilon(Term,NaEpsilon,NieNaEpsilon),
	proper_length(NieNaEpsilon,Dlugosc),
	Dlugosc == 1,
	przerob_produkcje(ResztaProdukcji,NaEpsilon,ResztaWyniku).
przerob_produkcje([Term|ResztaProdukcji], NaEpsilon, ResztaWyniku) :-
	\+ wszystkie_na_epsilon(Term, NaEpsilon),
	ile_nie_na_epsilon(Term,NaEpsilon,NieNaEpsilon),
	proper_length(NieNaEpsilon,Dlugosc),
	Dlugosc \= 1,
	przerob_produkcje(ResztaProdukcji,NaEpsilon,ResztaWyniku).

przerob_gramatyke([], _, []).
przerob_gramatyke([prod(N,Produkcja)|ResztaTranzycji], NaEpsilon, [prod(N,PrzerobionaProdukcja)|ResztaWyjscia]) :-
	przerob_produkcje(Produkcja, NaEpsilon, PrzerobionaProdukcja),
	PrzerobionaProdukcja \= [],
	przerob_gramatyke(ResztaTranzycji, NaEpsilon, ResztaWyjscia).
przerob_gramatyke([prod(_,Produkcja)|ResztaTranzycji], NaEpsilon, ResztaWyjscia) :-
	przerob_produkcje(Produkcja, NaEpsilon, PrzerobionaProdukcja),
	PrzerobionaProdukcja == [],
	przerob_gramatyke(ResztaTranzycji, NaEpsilon, ResztaWyjscia).

gramatyka_do_cyklu(GramatykaWejsciowa, GramatykaWyjsciowa) :-
	nieterminale_na_epsilon(GramatykaWejsciowa, NaEpsilon),
	gramatyka_usun_terminale(GramatykaWejsciowa, GramatykaBezTerminali),
	przerob_gramatyke(GramatykaBezTerminali,NaEpsilon, GramatykaWyjsciowa).

bezpiecznie_dodaj(El,Lista,Wyn) :-
	\+ member(El,Lista),
	append(Lista,[El],Wyn).
bezpiecznie_dodaj(El,Lista,Lista) :-
	member(El,Lista).

roznica_list([],_,[]).
roznica_list([El|ResztaListy], Lista, [El|Wyn]) :-
	\+ member(El, Lista),
	roznica_list(ResztaListy,Lista,Wyn).
roznica_list([El|ResztaListy], Lista, Wyn) :-
	member(El, Lista),
	roznica_list(ResztaListy,Lista,Wyn).

% tylko dla samych nieterminali
term_potomkowie([],[]).
term_potomkowie([nt(N)|ResztaTermu], Wyn2) :-
	term_potomkowie(ResztaTermu,Wyn),
	bezpiecznie_dodaj(N,Wyn,Wyn2).

produkcja_potomkowie([],[]).
produkcja_potomkowie([Term|ResztaProdukcji], Wynik) :-
	term_potomkowie(Term,Potomkowie),
	produkcja_potomkowie(ResztaProdukcji,Wyn),
	roznica_list(Potomkowie,Wyn,Roznica),
	append(Wyn,Roznica,Wynik).

% produkcja dla nieterminala
produkcja_dla_nieterminala(_, [], []).
produkcja_dla_nieterminala(N, [prod(N,Produkcja)|_], [prod(N,Produkcja)]).
produkcja_dla_nieterminala(N, [prod(M,_)|ResztaTranzycji], ResztaWyniku) :-
	M \= N,
	produkcja_dla_nieterminala(N,ResztaTranzycji,ResztaWyniku).

%odwiedz_liste(ListaDoOdwiedzenia, Gramatyka, ListaOdwiedzonych, ListaCykl)
odwiedz_liste([],_,_,_).
odwiedz_liste([N|_], Gramatyka, ListaOdwiedzonych, ListaCykl) :-
	produkcja_dla_nieterminala(N,Gramatyka, [Produkcja]),
	sprawdz_cykl(Produkcja,Gramatyka, ListaOdwiedzonych, ListaCykl).
odwiedz_liste([N|ResztaListy], Gramatyka, ListaOdwiedzonych, ListaCykl) :-
	produkcja_dla_nieterminala(N,Gramatyka, [Produkcja]),
	\+sprawdz_cykl(Produkcja,Gramatyka, ListaOdwiedzonych, ListaCykl),
	odwiedz_liste(ResztaListy, Gramatyka, ListaOdwiedzonych, ListaCykl).

% sprawdz_cykl dla jednej produkcji(produkcja, Gramatyka do cyklu,ListaOdwiedzonych, ListaCykl). - potem sie sprawdzi dlugosc listy
sprawdz_cykl(prod(N,Produkcja), Gramatyka, ListaOdwiedzonych, []) :-
	\+ member(N,ListaOdwiedzonych),
	append(ListaOdwiedzonych,[N],ListaOdwiedzonychN),
	produkcja_potomkowie(Produkcja, Potomkowie),
	odwiedz_liste(Potomkowie,Gramatyka,ListaOdwiedzonychN, []).
sprawdz_cykl(prod(N,_), _, ListaOdwiedzonych, []) :-
	member(N,ListaOdwiedzonych).

gramatyka_sprawdz_cykl([prod(N,Produkcja)|ResztaTranzycji],[]) :-
	sprawdz_cykl(prod(N,Produkcja),[prod(N,Produkcja)|ResztaTranzycji],[],_).
gramatyka_sprawdz_cykl([prod(N,Produkcja)|ResztaTranzycji],[]) :-
	\+ sprawdz_cykl(prod(N,Produkcja),[prod(N,Produkcja)|ResztaTranzycji],[],_),
	gramatyka_sprawdz_cykl(ResztaTranzycji,[]).

jestCykl(Gramatyka) :-
	gramatyka_do_cyklu(Gramatyka, GramatykaCykl),
	gramatyka_sprawdz_cykl(GramatykaCykl, _).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% z produkcji robi liste z pierszych terminali + ew epsilon
produkcja_eps_term([],[]).
produkcja_eps_term([[Pierwszy|_]|ResztaProdukcji], [Pierwszy|ResztaWyniku]) :-
	Pierwszy \= nt(_),
	produkcja_eps_term(ResztaProdukcji,ResztaWyniku).
produkcja_eps_term([[]|ResztaProdukcji], ResztaWyniku) :-
	produkcja_eps_term(ResztaProdukcji,ResztaWyniku).
produkcja_eps_term([[Pierwszy|_]|ResztaProdukcji], ResztaWyniku) :-
	Pierwszy = nt(_),
	produkcja_eps_term(ResztaProdukcji,ResztaWyniku).

gramatyka_eps_term([],[],[]).
gramatyka_eps_term([prod(N,Produkcja)|ResztaTranzycji], [first(N,Wynik)|ResztaWyniku], [N|Epsilon]) :-
	produkcja_eps_term(Produkcja, Wynik),
	member([],Produkcja),
	gramatyka_eps_term(ResztaTranzycji,ResztaWyniku, Epsilon).
gramatyka_eps_term([prod(N,Produkcja)|ResztaTranzycji], [first(N,Wynik)|ResztaWyniku], Epsilon) :-
	produkcja_eps_term(Produkcja, Wynik),
	\+ member([],Produkcja),
	gramatyka_eps_term(ResztaTranzycji,ResztaWyniku, Epsilon).

%znajdz_first (nieterminal, first, dany first, inne first)
znajdz_first(_,[],[],[]).
znajdz_first(N,[first(M,Wynik)|ResztaFirst],ResztaTo,[first(M,Wynik)|ResztaNieTo]) :-
	M \= N,
	znajdz_first(N,ResztaFirst,ResztaTo,ResztaNieTo).
znajdz_first(N,[first(N,Wynik)|ResztaFirst],[first(N,Wynik)|ResztaTo],ResztaNieTo) :-
	znajdz_first(N,ResztaFirst,ResztaTo,ResztaNieTo).

dodaj_do_first(first(N,First),Nowy, first(N,NowyFirst)) :-
	append(First,[Nowy],NowyFirst).
dodaj_do_first2(first(N,First),first(_,Nowy), first(N,NowyFirst)) :-
	append(First,Nowy,NowyFirst).

wszystkieMajaEps([],_).
wszystkieMajaEps([nt(N)|ResztaNieterminali], FirstEpsilon) :-
	member(N,FirstEpsilon),
	wszystkieMajaEps(ResztaNieterminali,FirstEpsilon).

first_obsluz_term(_,[],First,First, FirstEpsilon, FirstEpsilon, Wczesniej) :-
	Wczesniej = [].
first_obsluz_term(N,[],First,First, FirstEpsilon, [N|FirstEpsilon], Wczesniej) :-
	Wczesniej \= [],
	wszystkieMajaEps(Wczesniej,FirstEpsilon).
first_obsluz_term(_,[],First,First, FirstEpsilon, FirstEpsilon, Wczesniej) :-
	Wczesniej \= [],
	\+ wszystkieMajaEps(Wczesniej,FirstEpsilon).
first_obsluz_term(_,[[]],First,First, FirstEpsilon, FirstEpsilon, _).
first_obsluz_term(N,[Terminal|_],First,[FirstNNowy|NieFirstN],FirstEpsilon,FirstEpsilon,Wczesniej) :-
	Terminal \= [],
	Terminal \= nt(_),
	wszystkieMajaEps(Wczesniej,FirstEpsilon),
	znajdz_first(N,First,[FirstN],NieFirstN),
	dodaj_do_first(FirstN,Terminal,FirstNNowy),
	first_obsluz_term(N,[],First,First,FirstEpsilon,FirstEpsilon,[]).
first_obsluz_term(_,[Terminal|_],First,First,FirstEpsilon,FirstEpsilon,Wczesniej) :-
	Terminal \= [],
	Terminal \= nt(_),
	\+ wszystkieMajaEps(Wczesniej,FirstEpsilon).
first_obsluz_term(N,[nt(NT)|ResztaTermu],First,FirstCaly,FirstEpsilon,FirstEpsilonN,Wczesniej) :-
	wszystkieMajaEps(Wczesniej,FirstEpsilon),
	znajdz_first(N,First,[FirstN],NieFirstN),
	znajdz_first(NT,First,[FirstNT],_),
	dodaj_do_first2(FirstN,FirstNT,FirstNNT),
	first_obsluz_term(N,ResztaTermu,[FirstNNT|NieFirstN],FirstCaly,FirstEpsilon,FirstEpsilonN,[nt(NT)|Wczesniej]).
first_obsluz_term(_,[nt(_)|_],First,First,FirstEpsilon,FirstEpsilon,Wczesniej) :-
	\+ wszystkieMajaEps(Wczesniej,FirstEpsilon).

first_obsluz_produkcje(_,[],First,First,FirstEpsilon,FirstEpsilon).
first_obsluz_produkcje(N,[Term|ResztaProdukcji], First, FirstN, FirstEpsilon, FirstEpsilonN) :-
	first_obsluz_term(N,Term,First,FirstCaly,FirstEpsilon,FirstEpsilonCaly,[]),
	first_obsluz_produkcje(N,ResztaProdukcji,FirstCaly,FirstN,FirstEpsilonCaly,FirstEpsilonN).

first_obsluz_gramatyke([],First,First,FirstEpsilon,FirstEpsilon).
first_obsluz_gramatyke([prod(N,Produkcja)|ResztaTranzycji], First, FirstN, FirstEpsilon, FirstEpsilonN) :-
	first_obsluz_produkcje(N,Produkcja,First,FirstCaly,FirstEpsilon,FirstEpsilonCaly),
	first_obsluz_gramatyke(ResztaTranzycji,FirstCaly,FirstN,FirstEpsilonCaly,FirstEpsilonN).

usun_duplikaty_gramatyka([],[]).
usun_duplikaty_gramatyka([first(N,Produkcja)|ResztaTranzycji], [first(N,ProdukcjaWynik)|ResztaWyniku]) :-
	remove_dups(Produkcja,ProdukcjaWynik),
	usun_duplikaty_gramatyka(ResztaTranzycji,ResztaWyniku).
usun_duplikaty_gramatyka([follow(N,Produkcja)|ResztaTranzycji], [follow(N,ProdukcjaWynik)|ResztaWyniku]) :-
	remove_dups(Produkcja,ProdukcjaWynik),
	usun_duplikaty_gramatyka(ResztaTranzycji,ResztaWyniku).

punkt_staly(Gramatyka,First,FirstNDups,FirstEpsilon,FirstEpsilonNDups) :-
	first_obsluz_gramatyke(Gramatyka,First,FirstN, FirstEpsilon,FirstEpsilonN),
	usun_duplikaty_gramatyka(FirstN,FirstNDups),
	remove_dups(FirstEpsilonN,FirstEpsilonNDups),
	First = FirstNDups, FirstEpsilon = FirstEpsilonNDups.
punkt_staly(Gramatyka,First,FirstNN,FirstEpsilon,FirstEpsilonNN) :-
	first_obsluz_gramatyke(Gramatyka,First,FirstN, FirstEpsilon,FirstEpsilonN),
	usun_duplikaty_gramatyka(FirstN,FirstNDups),
	remove_dups(FirstEpsilonN,FirstEpsilonNDups),
	punkt_staly(Gramatyka,FirstNDups,FirstNN,FirstEpsilonNDups,FirstEpsilonNN).

dodaj_epsilony_first(First,[],First).
dodaj_epsilony_first(Firsts,[N|ResztaEps],NowyFirst2) :-
	znajdz_first(N,Firsts,[First],NieFirst),
	dodaj_do_first(First,'#',NowyFirst),
	dodaj_epsilony_first([NowyFirst|NieFirst], ResztaEps, NowyFirst2).

first(Gramatyka,FirstNNN) :-
	gramatyka_eps_term(Gramatyka,First,FirstEpsilon),
	first_obsluz_gramatyke(Gramatyka,First,FirstN, FirstEpsilon,FirstEpsilonN),
	punkt_staly(Gramatyka,FirstN,FirstNN,FirstEpsilonN,FirstEpsilonNN),
	dodaj_epsilony_first(FirstNN,FirstEpsilonNN,FirstNNN).

first1(Gramatyka,FirstNN) :-
	gramatyka_eps_term(Gramatyka,First,FirstEpsilon),
	first_obsluz_gramatyke(Gramatyka,First,FirstN, FirstEpsilon,FirstEpsilonN),
	punkt_staly(Gramatyka,FirstN,FirstNN,FirstEpsilonN,_).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

zainicjalizuj_follow([],[],_).
zainicjalizuj_follow([prod(N,_)|ResztaTranzycji], [follow(N,['#'])|ResztaWyniku],1) :-
	zainicjalizuj_follow(ResztaTranzycji,ResztaWyniku,0).
zainicjalizuj_follow([prod(N,_)|ResztaTranzycji], [follow(N,[])|ResztaWyniku],0) :-
	zainicjalizuj_follow(ResztaTranzycji,ResztaWyniku,0).

terminal(X) :- X \= nt(_).
nieterminal(X) :- X = nt(_).

%znajdz_follow (nieterminal, follows, dany follow, inne follow)
znajdz_follow(_,[],[],[]).
znajdz_follow(N,[follow(M,Wynik)|ResztaFollow],ResztaTo,[follow(M,Wynik)|ResztaNieTo]) :-
	M \= N,
	znajdz_follow(N,ResztaFollow,ResztaTo,ResztaNieTo).
znajdz_follow(N,[follow(N,Wynik)|ResztaFollow],[follow(N,Wynik)|ResztaTo],ResztaNieTo) :-
	znajdz_follow(N,ResztaFollow,ResztaTo,ResztaNieTo).

dodaj_do_follow(follow(N,Follow),Nowy, follow(N,NowyFollow)) :-
	append(Follow,[Nowy],NowyFollow).
dodaj_do_follow2(follow(N,Follow),follow(_,Nowy), follow(N,NowyFollow)) :-
	append(Follow,Nowy,NowyFollow).
dodaj_do_follow3(follow(N,Follow),Nowy, follow(N,NowyFollow)) :-
	append(Follow,Nowy,NowyFollow).

wyciagnij_first(first(_,First),FirstDel) :- delete(First,[],FirstDel).

% first_reszty(Do policzenia, NaEpsilon, Zbiory first, wynik)
first_reszty([],_,_,[]).
first_reszty([nt(N)|ResztaTermu],NaEpsilon,First,SamoResztaWyniku) :-
	member(N,NaEpsilon),
	znajdz_first(N,First,[FirstN],_),
	wyciagnij_first(FirstN,SamoFirstN),
	first_reszty(ResztaTermu,NaEpsilon,First,ResztaWyniku),
	append(SamoFirstN,ResztaWyniku,SamoResztaWyniku).
first_reszty([nt(N)|_],NaEpsilon,First,SamoResztaWyniku) :-
	\+ member(N,NaEpsilon),
	znajdz_first(N,First,[FirstN],_),
	wyciagnij_first(FirstN,SamoFirstN),
	first_reszty([],NaEpsilon,First,ResztaWyniku),
	append(SamoFirstN,ResztaWyniku,SamoResztaWyniku).
first_reszty([N|_],NaEpsilon,First,SamoResztaWyniku) :-
	terminal(N),
	first_reszty([],NaEpsilon,First,ResztaWyniku),
	append([N],ResztaWyniku,SamoResztaWyniku).

% follow_obsluz_term(Nieterminal, Term, First, NaEpsilon, Follow, FollowWyn)
follow_obsluz_term(_,[],_,_,Follow,Follow).
follow_obsluz_term(N, [nt(X),A|ResztaTermu],First,NaEpsilon, Follow, FollowCaly) :-
	terminal(A),
	znajdz_follow(X,Follow,[FollowN],FollowNieN),
	dodaj_do_follow(FollowN,A,FollowNA),
	follow_obsluz_term(N,ResztaTermu,First,NaEpsilon,[FollowNA|FollowNieN],FollowCaly).
follow_obsluz_term(N, [nt(X)|[]],First,NaEpsilon, Follow, FollowCaly) :-
	znajdz_follow(N,Follow,[FollowN],_),
	znajdz_follow(X,Follow,[FollowX],FollowNieX),
	dodaj_do_follow2(FollowX,FollowN,FollowNX),
	follow_obsluz_term(N,[],First,NaEpsilon,[FollowNX|FollowNieX],FollowCaly).
follow_obsluz_term(N, [nt(X)|ResztaTermu],First,NaEpsilon, Follow, FollowCaly) :-
	\+ wszystkie_na_epsilon(ResztaTermu,NaEpsilon),
	znajdz_follow(X,Follow,[FollowX],FollowNieX),
	first_reszty(ResztaTermu,NaEpsilon,First,FirstReszty),
	dodaj_do_follow3(FollowX,FirstReszty,FollowNX),
	follow_obsluz_term(N,ResztaTermu,First,NaEpsilon,[FollowNX|FollowNieX],FollowCaly).
follow_obsluz_term(N, [nt(X)|ResztaTermu],First,NaEpsilon, Follow, FollowCaly) :-
	wszystkie_na_epsilon(ResztaTermu,NaEpsilon),
	znajdz_follow(N,Follow,[FollowN],_),
	znajdz_follow(X,Follow,[FollowX],FollowNieX),
	dodaj_do_follow2(FollowX,FollowN,FollowNX),
	first_reszty(ResztaTermu,NaEpsilon,First,FirstReszty),
	dodaj_do_follow3(FollowNX,FirstReszty,FollowNXX),
	follow_obsluz_term(N,ResztaTermu,First,NaEpsilon,[FollowNXX|FollowNieX],FollowCaly).
follow_obsluz_term(N, [X|ResztaTermu],First,NaEpsilon, Follow, FollowCaly) :-
	terminal(X),
	follow_obsluz_term(N,ResztaTermu,First,NaEpsilon, Follow, FollowCaly).

follow_obsluz_produkcje(_,[],_,_,Follow,Follow).
follow_obsluz_produkcje(N,[Term|ResztaProdukcji],First,NaEpsilon,Follow,FollowCaly) :-
	follow_obsluz_term(N,Term,First,NaEpsilon,Follow,FollowN),
	follow_obsluz_produkcje(N,ResztaProdukcji,First,NaEpsilon,FollowN,FollowCaly).

follow_obsluz_gramatyke([],_,_,Follow,Follow).
follow_obsluz_gramatyke([prod(N,Produkcja)|ResztaTranzycji],First,NaEpsilon,Follow,FollowCaly) :-
	follow_obsluz_produkcje(N,Produkcja,First,NaEpsilon,Follow,FollowN),
	follow_obsluz_gramatyke(ResztaTranzycji,First,NaEpsilon,FollowN,FollowCaly).

zrob_obrot(Gramatyka,First,NaEpsilon,Follow,FollowNDups) :-
	follow_obsluz_gramatyke(Gramatyka,First,NaEpsilon,Follow,FollowN),
	usun_duplikaty_gramatyka(FollowN,FollowNDups).

identyczne(X,X).

punkt_staly_follow(Gramatyka,First,NaEpsilon,Follow,FollowNN) :-
	zrob_obrot(Gramatyka,First,NaEpsilon,Follow,FollowNDups),
	(Follow == FollowNDups -> 
		identyczne(FollowNDups,FollowNN);
		punkt_staly_follow(Gramatyka,First,NaEpsilon,FollowNDups,FollowNN)
	).

follow(Gramatyka,FollowN) :-
	zainicjalizuj_follow(Gramatyka,Follow,1),
	nieterminale_na_epsilon(Gramatyka,NaEpsilon),
	first1(Gramatyka,First),
	punkt_staly_follow(Gramatyka,First,NaEpsilon,Follow,FollowN).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

follow_dane(follow(_,Dane),Dane).

select_obsluz_term(N,Term,First1,Follow,NaEpsilon,Wynik) :-
	wszystkie_na_epsilon(Term,NaEpsilon),
	first_reszty(Term,NaEpsilon,First1,FirstReszty),
	znajdz_follow(N,Follow,[DanyFollow],_),
	follow_dane(DanyFollow,Dane),
	append(Dane,FirstReszty,WynikDups),
	remove_dups(WynikDups,Wynik).
select_obsluz_term(_,Term,First1,_,NaEpsilon,Wynik) :-
	\+ wszystkie_na_epsilon(Term,NaEpsilon),
	first_reszty(Term,NaEpsilon,First1,Wynik).

select_obsluz_produkcje(_,[],_,_,_,[]).
select_obsluz_produkcje(N,[Term|ResztaProdukcji],First1,Follow, NaEpsilon, [Wynik|ResztaWyniku]) :-
	select_obsluz_term(N,Term,First1,Follow,NaEpsilon,Wynik),
	select_obsluz_produkcje(N,ResztaProdukcji,First1,Follow,NaEpsilon,ResztaWyniku).

select_obsluz_gramatyke([],_,_,_,[]).
select_obsluz_gramatyke([prod(N,Produkcja)|ResztaTranzycji],First1,Follow, NaEpsilon, [Wynik|ResztaWyniku]) :-
	select_obsluz_produkcje(N,Produkcja,First1,Follow,NaEpsilon,Wynik),
	select_obsluz_gramatyke(ResztaTranzycji,First1,Follow,NaEpsilon,ResztaWyniku).

select(Gramatyka,Wynik) :-
	first1(Gramatyka,First1),
	follow(Gramatyka,Follow),
	nieterminale_na_epsilon(Gramatyka,NaEpsilon),
	select_obsluz_gramatyke(Gramatyka,First1,Follow,NaEpsilon,Wynik).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nieusuwalna_rekursja_w_gramatyce([prod(N, Produkcja)|Reszta]) :- 
    member([nt(N)|_],Produkcja); 
    nieusuwalna_rekursja_w_gramatyce(Reszta).

listy_rozlaczne([],_).
listy_rozlaczne([Poczatek|Reszta],Druga) :-
	\+ member(Poczatek,Druga),
	listy_rozlaczne(Reszta,Druga).

sprawdz_warunki([],_).
sprawdz_warunki([Poczatek|Reszta], Wynik) :-
	listy_rozlaczne(Poczatek,Wynik),
	append(Poczatek,Wynik,WynikN),
	sprawdz_warunki(Reszta,WynikN).

sprawdz_warunki_caly([]).
sprawdz_warunki_caly([Select|ResztaSelect]) :-
	sprawdz_warunki(Select,[]),
	sprawdz_warunki_caly(ResztaSelect).

jestLL1(Gramatyka) :-
	select(Gramatyka, Select),
	(sprawdz_warunki_caly(Select),\+ nieusuwalna_rekursja_w_gramatyce(Gramatyka) ->
		true;
		!,fail
	).



grammar(ex1, [prod('E', [[nt('E'), '+', nt('T')],  [nt('T')]]), prod('T', [[id],  ['(', nt('E'), ')']])   ]).
grammar(ex2, [prod('A', [[nt('A'), x], [x]])]).
grammar(ex3, [prod('A', [[x, nt('A')], [x]])]).
grammar(ex4, [prod('A', [[a, nt('B')], [a, nt('C')]]), prod('B', [[b]]), prod('C', [[c]])   ]).
grammar(ex5, [prod('A', [[a, nt('R')]]), prod('R', [[nt('B')], [nt('C')]]), prod('B', [[b]]), prod('C', [[c]])   ]).
grammar(ex6, [prod('S', [[nt('A'), a, nt('A'), b], [nt('B'), b, nt('B'), a]]), prod('A', [[]]), prod('B', [[]])    ]).
grammar(ex7, [prod('A', [[a], [nt('B'), x]]), prod('B', [[b], [nt('A'), y]]) ]).
grammar(ex8, [prod('A', [[nt('A'), a]]) ]).  
grammar(ex9, [prod('A', [[a, nt('B')], [a, nt('C')],[nt('D')]]), prod('B', [[b]]), prod('C', [[c]]), prod('D',[[nt('A')]])   ]).
grammar(ex10, [prod('A', [[nt('B')]]), prod('B', [[nt('C'),nt('A')]]), prod('C',[[nt('D')]]), prod('D',[[]])]).
grammar(ex11, [prod('S',[[nt('A'),nt('B'),nt('C'),nt('D'),nt('E')]]),prod('A',[['a'],[]]),prod('B',[['b'],[]]),prod('C',[['c']]),prod('D',[['d'],[]]),prod('E',[['e'],[]])]).
grammar(ex12, [prod('S',[[nt('B'),'b'],[nt('C'),'d']]),prod('B',[['a',nt('B')],[]]),prod('C',[['c',nt('C')],[]])]).
grammar(ex13, [prod('E',[[nt('T'),nt('E1')]]),prod('E1',[['+',nt('T'),nt('E1')],[]]),prod('T',[[nt('F'),nt('T1')]]),prod('T1',[['*',nt('F'),nt('T1')],[]]),prod('F',[['id'],['(',nt('E'),')']])]).
grammar(ex14, [prod('S',[[nt('A'),nt('C'),nt('B')],[nt('C'),'b',nt('B')],[nt('B'),'a']]),prod('A',[['d','a'],[nt('B'),nt('C')]]),prod('B',[['g'],[]]),prod('C',[['h'],[]])]).